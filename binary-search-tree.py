__author__ = "Josh Layton"
__email__ = "josh.l8n@gmail.com"


class Node(object):

    def __init__(self, data):
        self.left = None
        self.right = None
        self.data = data


class Tree(object):

    def __init__(self):
        self.root = None
        self.size = 0

    def insert(self, data):
        if self.exists(data):
            return False

        n = Node(data)
        self.root = self._insert(n, self.root)
        self.size += 1
        return True

    def _insert(self, n, current):
        if current is None:
            current = n
            return current
        elif n.data < current.data:
            current.left = self._insert(n, current.left)
            return current
        else:
            current.right = self._insert(n, current.right)
            return current

    def delete(self, data):
        if not self.exists(data):
            return False

        self._delete(data, self.root)
        self.size -= 1

        return True

    def _delete(self, data, current):
        if data < current.data:
            self._delete(data, current.left)
        elif data > current.data:
            self._delete(data, current.right)
        else:
            if not current.left and not current.right:  # no children
                current = None
            elif not current.left:
                current = current.right  # one right child
            elif not current.right:
                current = current.left  # one left child
            else:  # two children
                # find in-order successor
                s = current.right

                while s.left:
                    s = s.left

                current.data = s.data
                self._delete(s.data, current.right)

        return current

    def exists(self, data):
        exist = self._exists(data, self.root)
        return exist

    def _exists(self, data, current):
        if current is None:
            return False
        elif data < current.data:
            return self._exists(data, current.left)
        elif data > current.data:
            return self._exists(data, current.right)
        else:  # if data == current.data
            #print "already exists", current.data
            return True

    def traverse(self, callback):
        self._traverse(callback, self.root)

    def _traverse(self, callback, node):
        if node is None:
            return
        self._traverse(callback, node.left)
        callback(node.data)
        self._traverse(callback, node.right)

    def retrieve(self, item):
        if not self.exists(item):
            return None
        return self._retrieve(self.root, item)

    def _retrieve(self, current, item):
        if item < current.data:
            return self._retrieve(current.left, item)
        elif item > current.data:
            return self._retrieve(current.right, item)
        else:
            return current.data