__author__ = "Josh Layton"
__email__ = "josh.l8n@gmail.com"


class Stack:

    def __init__(self):
        self.stack = []

    def push(self, element):
        """Add an element to the top of the stack"""
        self.stack.append(element)
        return True

    def pop(self):
        """Remove and return the top-most element"""
        return self.stack.pop()

    def peek(self):
        """Return the top-most element"""
        if self.stack == []:
            # empty
            return None

        return self.stack[-1]

    def empty(self):
        """Return and empty the stack"""
        temp_stack = self.stack
        self.stack = []
        return temp_stack

    def print(self):
        for element in reversed(self.stack):
            print(element)
        
        return True
